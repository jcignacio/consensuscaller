'''
Created on Jul 31, 2019

@author: JIgnacio
'''

import math
import pandas as pd

class consensus:
    '''
    Class for consensus genotype calling
    '''

    def __init__(self, sample_data, genotype_data, group_column, sep, mis, threshold):
        '''
        Constructor
        '''
        if not isinstance(sample_data, pd.DataFrame):
            raise TypeError("sample_data must be a pandas dataframe")
        if not isinstance(genotype_data, pd.DataFrame):
            raise TypeError("genotype_data must be a pandas dataframe")
        self.sample_data = sample_data    
        self.genotype_data = genotype_data
        self.group_column = group_column
        self.sep = sep
        self.mis = mis
        self.threshold = threshold
        
    def get_consensus_data(self):
        return self.genotype_data, self.sample_data
    
    def create_encoding_dict(self, allele1, allele2, sep, mis):
        enc_dict = {
                allele1 + sep + allele1 : 0,
                allele1 + sep + allele2 : 1,
                allele2 + sep + allele1 : 2,
                allele2 + sep + allele2 : 3,
                mis + sep + mis : -1,
            }
        return enc_dict

    def create_recoding_dict(self, allele1, allele2, sep, mis):
        enc_dict = {
                0 : allele1 + sep + allele1,
                1 : allele1 + sep + allele2,
                2 : allele2 + sep + allele1,
                3 : allele2 + sep + allele2,
                -1 : mis + sep + mis,
            }
        return enc_dict
    
    def get_alleles(self, genotype_data, sep, mis):
        allele_matrix = {}
        for column in genotype_data:
            #get alleles
            alleles = list(set(sep.join(genotype_data[column].drop_duplicates()).split(sep)))
            if mis in alleles:
                alleles.remove(mis)
            while len(alleles) < 2:
                alleles.append('X')
            allele_matrix[column] = alleles
        return allele_matrix
    
    def count_genotype(self, variant):
        genotypes = [0,1,2,3]
        genotype_counts = []
        x = list(variant)
        for geno in genotypes:
            genotype_counts.append(x.count(geno))
        return genotype_counts
    
    def count_alleles(self, variant):
        #to_do
        print variant
    
    def encode_genotypes(self, genotype_data, sep, mis):
        for column in genotype_data:
            #get alleles
            alleles = list(set(sep.join(genotype_data[column].drop_duplicates()).split(sep)))
            #get encoding dictionary
            if mis in alleles:
                alleles.remove(mis)
            if len(alleles) == 2:
                encoding_dict = self.create_encoding_dict(alleles[0],alleles[1],sep,mis)
            else:
                encoding_dict = self.create_encoding_dict(alleles[0],'X',sep,mis)
            #encode genotypes
            genotype_data[column] = genotype_data[column].map(encoding_dict)
        return genotype_data
    
    def encode_genotypes_from_allele_matrix(self, genotype_data, allele_matrix, sep, mis):
        for column in genotype_data:
            #get alleles
            encoding_dict = self.create_encoding_dict(allele_matrix[column][0], allele_matrix[column][1], sep, mis)
            genotype_data[column] = genotype_data[column].map(encoding_dict)
        return genotype_data
    
    def recode_genotypes_from_allele_matrix(self, genotype_data, allele_matrix, sep, mis):
        for column in genotype_data:
            #get alleles
            recoding_dict = self.create_recoding_dict(allele_matrix[column][0], allele_matrix[column][1], sep, mis)
            genotype_data[column] = genotype_data[column].map(recoding_dict)
        return genotype_data
    
    def make_consensus(self):
        sample_data = self.sample_data
        genotype_data = self.genotype_data
        group_column = self.group_column
        sep = self.sep
        mis = self.mis
        threshold = self.threshold
        
        group_list = sample_data[group_column].drop_duplicates()
        allele_matrix = self.get_alleles(genotype_data, sep, mis)
        genotype_data = self.encode_genotypes_from_allele_matrix(genotype_data, allele_matrix, sep, mis)
        genotype_data = genotype_data.astype('int8')
        
        genotypes = [0,1,2,3,-1]
        
        tied_genotypes_dict = {
            '01' : 0,
            '02' : 0,
            '13' : 3,
            '23' : 3,
            '03' : -1,
            }
        
        column_names = [col for col in genotype_data]
        new_geno_data = pd.DataFrame(columns=column_names)
        new_sample_data = pd.DataFrame(columns=[col for col in sample_data]) 
        
        for index,item in group_list.iteritems():
            print item
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df_sample = sample_data[sample_data[group_column] == item]
            df_genotype = genotype_data.loc[df_sample.index.tolist(),:]
            
            count_matrix = {}
            freq_matrix = {}
            max_matrix = {}
            
            consensus_genotype = []
            
            for variant in df_genotype:
                count = self.count_genotype(df_genotype[variant])
                if sum(count) == 0:
                    consensus_genotype.append(-1)
                    continue
                freq = [x/float(sum(count)) for x in count]
                max_count = max(count)
                max_count_index = count.index(max_count)
                max_freq = freq[max_count_index]
                
                if max_freq > threshold: # set majority rule
                    consensus_genotype.append(genotypes[max_count_index])
                elif count.count(max_count) == 2: # look for 2-way tie and set according to dict
                    # get tied genotypes
                    tied_indices = [i for i, x in enumerate(count) if x == count[max_count_index]]
                    tied_genotypes = [str(genotypes[x]) for x in tied_indices]
                    consensus_genotype.append(tied_genotypes_dict["".join(tied_genotypes)])
                else: # set to missing
                    consensus_genotype.append(-1)
                        
                count_matrix[variant] = count
                freq_matrix[variant] = freq
                max_matrix[variant] = [max_count, max_count_index]
            
            df_genotype.loc[item+'*'] = consensus_genotype
            df_genotype.reindex()
            new_geno_data = pd.concat([new_geno_data,df_genotype], sort=False)
            
            tmp_df_sample = pd.DataFrame({group_column:item},index=[item+'*'])
            tmp_df_sample2 = pd.concat([df_sample,tmp_df_sample],axis=0, sort=False)
            new_sample_data = pd.concat([new_sample_data,tmp_df_sample2], sort=False)
                    
        new_geno_data = self.recode_genotypes_from_allele_matrix(new_geno_data, allele_matrix, sep, mis)
        
        self.genotype_data = new_geno_data
        self.sample_data = new_sample_data
        
        return self
    

